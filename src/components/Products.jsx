import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { addItem } from '../utils/cartSlice';
import { IoSearch } from "react-icons/io5";
import Navbar from './Navbar';

const Products = () => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [searchQuery, setSearchQuery] = useState('');
  const cart = useSelector((state) => state.cart.items);
  const dispatch = useDispatch();
  const API = "https://api.pujakaitem.com/api/products";

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const response = await fetch(API);
        if (!response.ok) {
          throw new Error('Failed to fetch products');
        }
        const data = await response.json();
        setProducts(data);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching products:', error);
        setLoading(false);
      }
    };

    fetchProducts();
  }, []);

  const filteredProducts = products.filter(product =>
    product.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const handleAddToCart = (product) => {
    dispatch(addItem(product));
  };

  return (
    <>
      <Navbar />

      {/* <div className="w-5/6 mx-auto"> */}
        <div className="flex items-center mx-[535px] w-[1300px]">
          <div className="flex items-center space-x-4 ">
            <input 
              type="text" 
              placeholder="Search" 
              className="w-96 px-4 py-2 text-gray-800 placeholder-gray-500 border border-gray-300 rounded-lg focus:outline-none focus:ring focus:border-purple-800" 
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
            />
            <button className="px-4 py-3 text-white bg-gray-400 hover:bg-purple-600 rounded-lg focus:outline-none focus:ring focus:border-purple-400"><IoSearch /></button>
          </div>
        </div>
      

      <div className="w-4/6 mx-auto py-12">
        {loading ? (
          <p className="text-center">Loading...</p>
        ) : (
          <div className="grid grid-cols-1 md:grid-cols-3 gap-8">
            {filteredProducts.map((product) => (
              <div key={product.id} className="bg-white shadow-lg rounded-lg overflow-hidden">
                <Link to={`/products/${product.id}`} className="relative">
                  <img src={product.image} alt={product.title} className="w-full h-34 object-cover transition duration-300 transform hover:scale-110" />
                  <div className="absolute inset-0 bg-black bg-opacity-50 flex items-center justify-center opacity-0 transition duration-300 hover:opacity-100">
                    <p className="text-white text-center">View Details</p>
                  </div>
                </Link>
                <div className=" flex py-2 justify-between px-3">
                  <h3 className="font-semibold  mb-2 text-black">{product.name}</h3>
                  <p className=" font-semibold text-purple-500">${product.price}</p>
                </div>
                <button onClick={() => handleAddToCart(product)} className="w-full bg-purple-500 text-white font-semibold py-2 px-4 rounded-b">Add to Cart</button>
              </div>
            ))}
          </div>
        )}
      </div>
    </>
  );
};

export default Products;
