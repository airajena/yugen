import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./cartSlice";
import productReducer from "./productSlice";

const initialState = {
  products: [],
};

const rootReducer = {
  cart: cartReducer,
  products: productReducer,
};

const appStore = configureStore({
  reducer: rootReducer,
  preloadedState: initialState,
});

export default appStore;
